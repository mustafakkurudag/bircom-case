<?php

namespace Container3QbtPz9;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2a28d = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerdf6a9 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesc5988 = [
        
    ];

    public function getConnection()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getConnection', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getMetadataFactory', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getExpressionBuilder', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'beginTransaction', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getCache', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getCache();
    }

    public function transactional($func)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'transactional', array('func' => $func), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->transactional($func);
    }

    public function commit()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'commit', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->commit();
    }

    public function rollback()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'rollback', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getClassMetadata', array('className' => $className), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'createQuery', array('dql' => $dql), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'createNamedQuery', array('name' => $name), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'createQueryBuilder', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'flush', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'clear', array('entityName' => $entityName), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->clear($entityName);
    }

    public function close()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'close', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->close();
    }

    public function persist($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'persist', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'remove', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'refresh', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'detach', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'merge', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getRepository', array('entityName' => $entityName), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'contains', array('entity' => $entity), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getEventManager', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getConfiguration', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'isOpen', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getUnitOfWork', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getProxyFactory', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'initializeObject', array('obj' => $obj), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'getFilters', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'isFiltersStateClean', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'hasFilters', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return $this->valueHolder2a28d->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerdf6a9 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder2a28d) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder2a28d = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder2a28d->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__get', ['name' => $name], $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        if (isset(self::$publicPropertiesc5988[$name])) {
            return $this->valueHolder2a28d->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2a28d;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2a28d;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2a28d;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2a28d;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__isset', array('name' => $name), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2a28d;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2a28d;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__unset', array('name' => $name), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2a28d;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2a28d;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__clone', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        $this->valueHolder2a28d = clone $this->valueHolder2a28d;
    }

    public function __sleep()
    {
        $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, '__sleep', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;

        return array('valueHolder2a28d');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerdf6a9 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerdf6a9;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerdf6a9 && ($this->initializerdf6a9->__invoke($valueHolder2a28d, $this, 'initializeProxy', array(), $this->initializerdf6a9) || 1) && $this->valueHolder2a28d = $valueHolder2a28d;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2a28d;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2a28d;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
