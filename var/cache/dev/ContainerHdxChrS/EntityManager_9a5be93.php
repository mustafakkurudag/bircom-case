<?php

namespace ContainerHdxChrS;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderf366c = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer6006c = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties13dd6 = [
        
    ];

    public function getConnection()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getConnection', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getMetadataFactory', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getExpressionBuilder', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'beginTransaction', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getCache', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getCache();
    }

    public function transactional($func)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'transactional', array('func' => $func), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->transactional($func);
    }

    public function commit()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'commit', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->commit();
    }

    public function rollback()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'rollback', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getClassMetadata', array('className' => $className), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'createQuery', array('dql' => $dql), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'createNamedQuery', array('name' => $name), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'createQueryBuilder', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'flush', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'clear', array('entityName' => $entityName), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->clear($entityName);
    }

    public function close()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'close', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->close();
    }

    public function persist($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'persist', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'remove', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'refresh', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'detach', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'merge', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getRepository', array('entityName' => $entityName), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'contains', array('entity' => $entity), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getEventManager', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getConfiguration', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'isOpen', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getUnitOfWork', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getProxyFactory', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'initializeObject', array('obj' => $obj), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'getFilters', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'isFiltersStateClean', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'hasFilters', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return $this->valueHolderf366c->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer6006c = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderf366c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderf366c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderf366c->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__get', ['name' => $name], $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        if (isset(self::$publicProperties13dd6[$name])) {
            return $this->valueHolderf366c->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf366c;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf366c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf366c;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf366c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__isset', array('name' => $name), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf366c;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderf366c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__unset', array('name' => $name), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf366c;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderf366c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__clone', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        $this->valueHolderf366c = clone $this->valueHolderf366c;
    }

    public function __sleep()
    {
        $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, '__sleep', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;

        return array('valueHolderf366c');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer6006c = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer6006c;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer6006c && ($this->initializer6006c->__invoke($valueHolderf366c, $this, 'initializeProxy', array(), $this->initializer6006c) || 1) && $this->valueHolderf366c = $valueHolderf366c;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderf366c;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderf366c;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
