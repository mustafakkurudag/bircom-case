<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* movie/index.html.twig */
class __TwigTemplate_d59489935385d8002e1834d3fa3ab3d190eafcaaab1606619949e9b0d0e91487 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "movie/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "movie/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "movie/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Movie index";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <main class=\"main-content\">
        <div class=\"container\">
            <div class=\"page\">
                <div class=\"breadcrumbs\">
                    <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("movie_index");
        echo "\">Home</a>
                </div>

                <div class=\"movie-list\">

                    ";
        // line 16
        if (twig_test_empty((isset($context["movies"]) || array_key_exists("movies", $context) ? $context["movies"] : (function () { throw new RuntimeError('Variable "movies" does not exist.', 16, $this->source); })()))) {
            // line 17
            echo "                        <p>
                            The database is empty. If you want to see the movies,
                            you need to go to the project folder at the command prompt(cmd)
                            and run the <b>php bin\\console fetch-movies</b> command. </p>
                        <p>
                            Veritabanı boş. Eğer filmleri görmek isterseniz komut isteminde(cmd) proje klasörüne gelerek
                            <b>php bin\\console fetch-movies</b> komutunu çalıştırmanız gerekmektedir.
                        </p>
                    ";
        } else {
            // line 26
            echo "
                    ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["movies"]) || array_key_exists("movies", $context) ? $context["movies"] : (function () { throw new RuntimeError('Variable "movies" does not exist.', 27, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["movie"]) {
                // line 28
                echo "                    <div class=\"movie\">
                        <figure class=\"movie-poster\">
                            <img src=\"";
                // line 30
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["movie"], "image", [], "any", false, false, false, 30), "html", null, true);
                echo "\" alt=\"#\">
                        </figure>
                        <div class=\"movie-title\">
                            <a href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("movie_show", ["name" => twig_get_attribute($this->env, $this->source, $context["movie"], "name", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["movie"], "name", [], "any", false, false, false, 33), "html", null, true);
                echo "</a>
                        </div>
                        <p>";
                // line 35
                echo twig_get_attribute($this->env, $this->source, $this->extensions['Twig\Extra\String\StringExtension']->createUnicodeString(twig_get_attribute($this->env, $this->source, $context["movie"], "summary", [], "any", false, false, false, 35)), "truncate", [0 => 70], "method", false, false, false, 35);
                echo "...</p>
                    </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['movie'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                    ";
        }
        // line 39
        echo "                </div> <!-- .movie-list -->

            </div>
        </div> <!-- .container -->
    </main>
</div>
    <!-- Default snippet for navigation -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "movie/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 39,  149 => 38,  140 => 35,  133 => 33,  127 => 30,  123 => 28,  119 => 27,  116 => 26,  105 => 17,  103 => 16,  95 => 11,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Movie index{% endblock %}

{% block body %}

    <main class=\"main-content\">
        <div class=\"container\">
            <div class=\"page\">
                <div class=\"breadcrumbs\">
                    <a href=\"{{ path('movie_index') }}\">Home</a>
                </div>

                <div class=\"movie-list\">

                    {% if movies is empty %}
                        <p>
                            The database is empty. If you want to see the movies,
                            you need to go to the project folder at the command prompt(cmd)
                            and run the <b>php bin\\console fetch-movies</b> command. </p>
                        <p>
                            Veritabanı boş. Eğer filmleri görmek isterseniz komut isteminde(cmd) proje klasörüne gelerek
                            <b>php bin\\console fetch-movies</b> komutunu çalıştırmanız gerekmektedir.
                        </p>
                    {% else %}

                    {% for movie in movies %}
                    <div class=\"movie\">
                        <figure class=\"movie-poster\">
                            <img src=\"{{ movie.image }}\" alt=\"#\">
                        </figure>
                        <div class=\"movie-title\">
                            <a href=\"{{ path('movie_show', {'name':movie.name} ) }}\">{{ movie.name }}</a>
                        </div>
                        <p>{{ movie.summary | u.truncate(70) | raw }}...</p>
                    </div>
                    {% endfor %}
                    {% endif %}
                </div> <!-- .movie-list -->

            </div>
        </div> <!-- .container -->
    </main>
</div>
    <!-- Default snippet for navigation -->
{% endblock %}
", "movie/index.html.twig", "C:\\xampp\\htdocs\\bircom_project\\templates\\movie\\index.html.twig");
    }
}
