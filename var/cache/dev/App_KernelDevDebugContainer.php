<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerHdxChrS\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerHdxChrS/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerHdxChrS.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerHdxChrS\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerHdxChrS\App_KernelDevDebugContainer([
    'container.build_hash' => 'HdxChrS',
    'container.build_id' => '4c5b3f18',
    'container.build_time' => 1631359153,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerHdxChrS');
