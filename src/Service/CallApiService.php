<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function fetchMoviesData(): array
    {
        $response = $this->client->request(
            'GET',
            'http://api.tvmaze.com/search/shows?q=comedy'
        );

        return $response->toArray();

    }
}