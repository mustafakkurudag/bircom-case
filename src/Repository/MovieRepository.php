<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Service\CallApiService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    private $client;
    private $conn;

    public function __construct(Connection $conn, ManagerRegistry $registry, HttpClientInterface $client)
    {
        $this->client = $client;
        $this->conn = $conn;

        parent::__construct($registry, Movie::class);

    }

    // /**
    //  * @return Movie[] Returns an array of Movie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Movie
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    //this method writes the data to the database
    public function commandExecutor(){

        $callApiService = new CallApiService($this->client);
        $movies = $callApiService->fetchMoviesData();

        foreach ($movies as $moviesData){

            $nameData = $moviesData['show']['name'];

            $mediumImageData = $moviesData['show']['image']['medium'];

            $originalImageData = $moviesData['show']['image']['original'];

            $typeData = $moviesData['show']['type'];

            $summaryData = $moviesData['show']['summary'];
            //summaryData has special characters. I removed these characters because they were problem for mysql database
            $summaryDataNew = str_replace("'", "", $summaryData);

            $languageData = $moviesData['show']['language'];

            $typeData = $moviesData['show']['type'];

            //some datas are different in json api. so I wrote following if else blocks.
            if (isset($moviesData['show']['network']['country']['name'])) {
                $countryData = $moviesData['show']['network']['country']['name'];
            } else {
                $countryData = $moviesData['show']['webChannel']['country']['name'];
            }

            $premiereData = $moviesData['show']['premiered'];

            $lengthData = $moviesData['show']['runtime'];

            $query = "INSERT INTO movie(name, type, language, image, image_original, country, premiere, length, summary )
                    VALUES ('$nameData', '$typeData', '$languageData', '$mediumImageData', 
                            '$originalImageData', '$countryData', '$premiereData', '$lengthData', '$summaryDataNew')";

            $this->conn->executeQuery($query);

        }
    }
}
