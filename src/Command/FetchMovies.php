<?php

namespace App\Command;

use App\Repository\MovieRepository;
use App\Service\CallApiService;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class FetchMovies extends Command
{
    private $conn;
    private $movieRepository;

    public function __construct(Connection $conn, MovieRepository $movieRepository)
    {
        $this->conn = $conn;
        $this->movieRepository = $movieRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('fetch-movies')
            ->setDescription('Saves movies to database')
            ->setHelp('This command fetches the movies on database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            '========================'
        );

        //calling database writter method
        $this->movieRepository->commandExecutor();

        $output->writeln(
            'All movies fetched to database.'
        );
        $output->writeln(
            'You can check the home page now.'
        );

        return Command::SUCCESS;
    }

}